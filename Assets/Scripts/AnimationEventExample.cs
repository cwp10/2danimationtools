﻿using UnityEngine;
using System.Collections;

public class AnimationEventExample : MonoBehaviour
{
	public SortingGroup sortingGroup;
	public Renderer renderer;

	public void MoveRendererToBack()
	{
		if (sortingGroup == null || renderer == null)
			return;

		sortingGroup.MoveToBack (renderer);
	}

	public void MoveRendererToFront()
	{
		if (sortingGroup == null || renderer == null)
			return;

		sortingGroup.MoveToFront (renderer);
	}
}
